const container = document.querySelector(".container");
const generateBtn = document.querySelector(".generate-btn");

const numColors = 16;

const generatePalette = () => {
    const baseColor = document.querySelector(".color").value;
    const saturation = document.querySelector(".range_1").value;
    const lightness = document.querySelector(".range_2").value;
    const rgbColor = hexToRgb(baseColor);
    const hslBase = rgbToHsl(rgbColor);
    container.innerHTML = "";
    for (let i = 0; i < numColors; i++) {
        const hue = (hslBase[0] + (i * 15)) % 360;
        const [r, g, b] = hslToRgb(hue, saturation, lightness);
        const hexVal = rgbToHex(r, g, b);
        const rgbVal = hexToRgb(hexVal);
        const hslVal = rgbToHsl([r, g, b]);

        const color = document.createElement("li");
        color.classList.add("color");
        color.innerHTML = `<div class="rect-box" style="background: rgb(${rgbVal})"></div>
                        <div class="color-value" id="hex">HEX: ${hexVal}</div>
                        <div class="color-value" id="rgb">RGB: rgb(${rgbVal})</div>
                        <div class="color-value" id="hsl">HSL: hsl(${hslVal})</div>`;

        container.appendChild(color);
    }
}

generatePalette();

generateBtn.addEventListener("click", generatePalette);

function hexToRgb(hex) {
    hex = hex.replace("#", "");

    const r = parseInt(hex.substring(0, 2), 16);
    const g = parseInt(hex.substring(2, 4), 16);
    const b = parseInt(hex.substring(4, 6), 16);

    return [r, g, b];
}

function rgbToHex(r, g, b) {
    var red = r.toString(16);
    var green = g.toString(16);
    var blue = b.toString(16);
  
    if (red.length == 1) red = "0" + red;
    if (green.length == 1) green = "0" + green;
    if (blue.length == 1) blue = "0" + blue;
  
    return "#" + red + green + blue;
  }

function rgbToHsl(rgb) {
    const r = rgb[0] / 255;
    const g = rgb[1] / 255;
    const b = rgb[2] / 255;

    const max = Math.max(r, g, b);
    const min = Math.min(r, g, b);

    let h, s, l = (max + min) / 2;

    if (max === min) {
        h = s = 0; // achromatic
    } else {
        const d = max - min;
        s = l > 0.5 ? d / (2 - max - min) : d / (max + min);

        switch (max) {
            case r:
                h = (g - b) / d + (g < b ? 6 : 0);
                break;
            case g:
                h = (b - r) / d + 2;
                break;
            case b:
                h = (r - g) / d + 4;
                break;
        }

        h /= 6;
    }

    h = +(h * 360).toFixed(2);
    s = +(s * 100).toFixed(2);
    l = +(l * 100).toFixed(2);

    return [h, s, l];
}

function hslToRgb(h, s, l) {
    const c = (1 - Math.abs((2 * l / 100) - 1)) * (s / 100);
    const x = c * (1 - Math.abs((h / 60) % 2 - 1));
    const m = (l / 100) - (c / 2);

    let r, g, b;

    if (h < 60) {
        r = c;
        g = x;
        b = 0;
    } else if (h < 120) {
        r = x;
        g = c;
        b = 0;
    } else if (h < 180) {
        r = 0;
        g = c;
        b = x;
    } else if (h < 240) {
        r = 0;
        g = x;
        b = c;
    } else if (h < 300) {
        r = x;
        g = 0;
        b = c;
    } else {
        r = c;
        g = 0;
        b = x;
    }

    r = Math.round((r + m) * 255);
    g = Math.round((g + m) * 255);
    b = Math.round((b + m) * 255);

    return [r, g, b];
}