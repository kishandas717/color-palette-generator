# Color Palette Generator

A simple website for generating color palettes using a single base color. It also allows to choose the saturation and lightness range for the palettes.