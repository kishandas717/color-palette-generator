const color = document.querySelector(".color");
const slider_1 = document.querySelector(".range_1");
const slider_2 = document.querySelector(".range_2");

const color_value = document.querySelector("#color_value")
const value_1 = document.querySelector("#value_1");
const value_2 = document.querySelector("#value_2");

color_value.textContent = color.value;
value_1.textContent = slider_1.value;
value_2.textContent = slider_2.value;

color.oninput = function() {
    color_value.textContent = this.value;
}

slider_1.oninput = function() {
    value_1.textContent = this.value;
}

slider_2.oninput = function() {
    value_2.textContent = this.value;
}

